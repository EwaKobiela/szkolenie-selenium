This project serves as a template for Selenium Trainings conducted by @EwaKobiela and @jswartho.

The master branch contains files for the beginers part of the training.

The branch "zaawansowane" was added for the advanced part of the training.

In order to participate in the training, please download the appropriate branch to your personal machine (no need to clone it).
